set commands {
   {{(?i)((\m[[:graph:]]*\M),)+ (\m[[:graph:]]*\M),? or (.*)\?$} {_ _ string1 string2 string3} {
         set possibilities [split $string1 {, }]
         set possibilities [lsearch -all -inline -not -exact $possibilities {}]
         lappend possibilities $string2 $string3
         return [lrandom $possibilities]
   }}
   {{(?i)(.*) or (.*)\?$} {_ first second} {
         if {$first eq $second} {
            return "it's the same"
         }
         if {[rand_int 0 5] == 0} {
            return "why not both?"
         }
         if {[rand_int 0 1]} {
            return $first
         } else {
            return $second
         }
   }}
   {{(?i)(.*) is an insulting noun$} {_ noun} {
         global insult_noun
         lappend insult_noun $noun
         return ok
   }}
   {{(?i)(.*) is an insulting adj(ective)?$} {_ adj _} {
         global insult_adj
         lappend insult_adj $adj
         return ok
   }}
   {{(?i)(.*) is an insulting verb$} {_ verb} {
         global insult_verb
         lappend insult_verb $verb
         return ok
   }}
   {{(?i)insult( ([^\n]*))?} {_ _ person} {
         global peernames
         if {[regexp {^(jas|jejs)(mine)?( .*)?$} $person]} {
            return "no."
         }
         if {[regexp {^zcm$} $person]} {
            return "no."
         }
         if {$person eq "anyone"} {
            set person [lrandom $peernames]
         }
         if {$person eq "yourself"} {
            set person "jasmine"
         }
         if {$person eq "me"} {
            set person "you"
         }
         
         if {$person eq "" } {
            return [insult_eval]
         } else {
            return [insult_eval $person]
         }
   }}
   {{(?i).*\m(about|of|like|the|a|an|that|this|with|to|any|some|is|on|at|in|your|my|for)\M \m([[:graph:]]*)\M} {_ _ subject} {
         set substituted_nicks [nicks2tags subject]
         set possible_firsts [array names firsts -glob $subject]
         set possible_nexts [array names nexts -regexp "(?i)^(.* )?$subject\( .*\)?$"]

         if {$possible_nexts ne ""} {
            set res [lrandom $possible_nexts]
            set res "$res [say_word {*}$res]"

            tags2nicks res {*}$substituted_nicks

            return $res
         } else {
            return "[say_sentence] ."
         }
   }}
   {{(?i)\mlove you\M} {_} {
         return "I love you too"
   }}
   {{(?i)call} {_} {
         return $peernames
   }}
   {{(?i)ignore (.*)} {_ person} {
         return ok
   }}
   {{(?i)load} {_} {
         global dumppath
         global firsts nexts insult_noun insult_adj insult_verb
         source -encoding utf-8 $dumppath/firsts.dump
         source -encoding utf-8 $dumppath/nexts.dump
         source -encoding utf-8 $dumppath/insult_noun.dump
         source -encoding utf-8 $dumppath/insult_adj.dump
         source -encoding utf-8 $dumppath/insult_verb.dump
         return ok
   }}
   {{(?i)dump} {_} {
         global dumppath
         global insult_noun insult_adj insult_verb
         arr'dump firsts $dumppath/firsts.dump
         arr'dump nexts $dumppath/nexts.dump
         list'dump insult_noun $dumppath/insult_noun.dump
         list'dump insult_adj $dumppath/insult_adj.dump
         list'dump insult_verb $dumppath/insult_verb.dump
         return ok
   }}
   {{} {_} {
         return [say_sentence]
   }}
}

set commands_special {
   {{(?i)jejsmine} {_} {
         if {[rand_int 1 5] == 1} {
            return [lrandom {
               {oh fuck you}
               {stop calling me jejsmine}
               {plz stahp}
               {my name is Jasmine, but you can call me jas :)}
               {OH GOD I LOVE WHEN YOU'RE CALLING ME JEJSMINE OOOH IM CUMMING RIGHT NNOW}
               {no bully pls}
               {not funny}
               {are you autistic or what?}
            }]
         } else {
            return [insult_eval]
         }
   }}
}
