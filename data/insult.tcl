set insult_suggestion {
   {get lost}
   {jump off a bridge}
   {go away}
   {go <insult_verb> yourself}
   {<insult_verb> yourself}
   {go <insult_verb> a meat grinder}
   {<insult_verb> <a-or-an><insult_simple>}
}

set insult_simple {
   {<insult_adj> <insult_noun>}
   {<insult_adj> <insult_noun>}
   {<insult_adj> <insult_noun>}
   {<insult_noun>}
   {<insult_adj>, <insult_adj> <insult_noun>}
   {<insult_noun> enthusiast}
}

set insult_special {
   {} {} {} {} {} {} {} {} {} {}
   {The only way <person> will ever get laid is if <person-subject-pronoun> crawl up a chicken's ass and wait.}
   {<person><is-or-are> so ugly, when <person-possessive-pronoun> mom dropped <person-object-pronoun> off at school she got a fine for littering.}
   {<person-possessive> family tree must be a cactus because everybody on it is a prick.}
   {Two wrongs don't make a right, take <person-possessive> parents as an example.}
   {I wasn't born with enough middle fingers to let <person> know how I feel about <person-object-pronoun>.}
   {<person><is-or-are> so fake, Barbie is jealous.}
   {I'm jealous of all the people that haven't met <person>!}
   {Which sexual position produces the ugliest children? Ask <person-possessive> mother.}
   {If bullshit could float, <person> would be the Admiral of the fleet!}
   {I would love to insult <person-object-pronoun>, but that would be beyond the level of <person-possessive-pronoun> intelligence.}
   {Compared with <person>, being <insult_simple> doesn't seem so bad.}
   {I'd rather <insult_verb> with <a-or-an><insult_simple> to <insult_simple> porn than be friends with <person>.}
}

set insult_comment {
   {}
   {... <person-subject-pronoun>, and the horse <person-subject-pronoun> rode in on}
   {, and anybody who likes <person-object-pronoun> is one too}
   {, <insult_simple>! <insult_adj>, <insult_adj>, <insult_simple>}
   {, and the offspring of <a-or-an><insult_simple>}
   { - <person-subject-pronoun><is-or-are> the <insult_adj> gold standard for it}
   {, and <person-possessive> mother's another one}
   {, I hope <person-possessive> <insult_adj>, <insult_adj> dog dies}
   {, <person-subject-pronoun>, AND <person-possessive-pronoun> WHOLE GRADUATING CLASS}
}

set insult {
 {<person><is-or-are> <a-or-an><insult_simple><insult_comment>!}
 {<person><is-or-are> <a-or-an><insult_simple><insult_comment>! <insult_special>}
 {<person><is-or-are> <a-or-an><insult_simple>. <insult_special>}
 {<person>: <insult_suggestion>, you <insult_simple>}
 {<person>: I know that you're <a-or-an><insult_simple>, but holy jesus, <insult_suggestion>.}
 {<person> look<s?> like <a-or-an><insult_simple>}
 {<person-possessive> mom is <a-or-an><insult_simple>. <insult_special>}
 {<person>: you <insult_simple>. <insult_special>}
}

set insult_data {
   insult_simple
   insult_adj
   insult_noun
   insult_verb
   insult_comment
   insult_suggestion
   insult_special
}

proc escapeSubSpec {string} {
   return [regsub -all -- {[\\&]} $string {\\&}]
}

proc insult_eval {{person you}} {
   variable insult
   variable insult_data
   foreach i_d $insult_data {
      variable $i_d
   }

   set msg [lrandom $insult]

   while 1 {
      set counter 0
      foreach i_d $insult_data {
         if {[regsub -- "<$i_d>" $msg [escapeSubSpec [lrandom [set $i_d]]] msg] > 0} {
            incr counter
         }
      }
      if {$counter == 0} { break }
   }

   while 1 {
      set counter 0
      if {[regsub -- {<a-or-an>([aeiou])} $msg {an \1} msg] > 0} {
         incr counter
      }
      if {[regsub -- {<a-or-an>([^aeiou])} $msg {a \1} msg] > 0} {
         incr counter
      }
      if {$counter == 0} { break }
   }

   regsub -all -- {<person>} $msg $person msg
   if {"$person" eq "you"} {
      regsub -all -- {<person-possessive>} $msg {your} msg
      regsub -all -- {<person-subject-pronoun>} $msg {you} msg
      regsub -all -- {<person-object-pronoun>} $msg {you} msg
      regsub -all -- {<person-possessive-pronoun>} $msg {your} msg
      regsub -all -- {<s\?>} $msg {} msg
   } else {
      regsub -all -- {<person-possessive>} $msg "[escapeSubSpec [set person]]'s" msg
      regsub -all -- {<person-subject-pronoun>} $msg {he} msg
      regsub -all -- {<person-object-pronoun>} $msg {him} msg
      regsub -all -- {<person-possessive-pronoun>} $msg {his} msg
      regsub -all -- {<s\?>} $msg {s} msg
   }

   regsub -all -- {(you)<is-or-are>} $msg {\1 are} msg
   regsub -all -- {<is-or-are>} $msg { is} msg

   return $msg
}

