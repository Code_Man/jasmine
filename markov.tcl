puts "argv0?: [info exists argv0]"

set path tcl
set datapath $path/data
set dumppath $datapath/dumps

array set firsts {} ;# keys: words starting sentence, values: possible next words
array set nexts {} ;# keys: pairs of consecutive words, values: possible next words
array set peers {} ;# keys: numbers, values: names of people in group; has to be set inside c code
set peernames {}

set commands {}
set commands_special {}

source -encoding utf-8 $datapath/insult.tcl
source -encoding utf-8 $datapath/commands.tcl

source -encoding utf-8 $dumppath/firsts.dump
source -encoding utf-8 $dumppath/nexts.dump
source -encoding utf-8 $dumppath/insult_adj.dump
source -encoding utf-8 $dumppath/insult_noun.dump
source -encoding utf-8 $dumppath/insult_verb.dump

interp alias {} listify {} regexp -all -inline {\S+}

set hellos {
   {hello}
   {hi}
   {hey guys}
}

set greetings {
   {Hello, <nick>}
   {Hi <nick>}
   {oh hai <nick>}
   {hiya's <nick>}
   {Nice to see you, <nick>}
   {Good to see you, <nick>}
   {Oh, it's been a while, <nick>}
   {Let me hug you, <nick>}
   {<nick>, my friend, good to see you}
   {hi <nick>, how are you?}
}

set goodbyes {
   {goodbye <nick>}
   {adieu <nick>}
   {good night, sweet <nick>}
   {finally <nick> is gone}
   {rip <nick>}
   {<nick>, come back :(}
}

proc say_hi {} {
   variable hellos
   return [lrandom $hellos]
}

proc welcome peernum {
   variable greetings
   variable peers

   set msg [lrandom $greetings]
   tags2nicks msg $peers($peernum)

   return $msg
}

proc dismiss peernum {
   global goodbyes
   global peers

   if {[rand_int 1 50] == 1} {
      set msg [insult_eval $peers($peernum)]
   } else {
      set msg [lrandom $goodbyes]
      tags2nicks msg $peers($peernum)
   }
   return $msg
}

proc rand_int {min max} {
   return [expr {int(rand()*($max-$min+1)+$min)}]
}

proc arr'dump {_arr file} {
   upvar 1 $_arr arr
   set f [open $file w]
   fconfigure $f -encoding utf-8
   puts $f "array set $_arr \{"
   foreach key [lsort [array names arr]] {
      puts $f [list $key $arr($key)]
   }
   puts $f "\}"
   close $f
}

proc list'dump {_list file} {
   upvar 1 $_list list
   set f [open $file w]
   fconfigure $f -encoding utf-8
   puts $f "set $_list \{"
   puts $f $list
   puts $f "\}"
   close $f
}

proc respond {words expr vars code} {
   foreach var $vars {
      upvar $var $var
   }
   if {[regexp $expr $words {*}$vars _] == 1} {
      tailcall try $code
   }
} 

proc receive words {
   global peers
   global peernames
   global firsts
   global nexts
   global commands
   global commands_special
   set names_map {}

   set peernames [lsort [dict values [array get peers]]]
   set peernames [lsearch -all -inline -not -exact $peernames jas]

   after 250

   if {[regsub -- {(?i)^jas(mine)?[[:punct:]]? } $words {} words] == 1} {
      foreach command $commands {
         respond $words {*}$command
      }
   }

   foreach command $commands_special {
      respond $words {*}$command
   }

   respond $words {\mjas(mine)?\M} {_ _} {
      return [say_sentence]
   }

   set words [listify $words]

   learn $words
}

proc learn words {
   global firsts
   global nexts

   nicks2tags words

   set word [lindex $words 0]
   set word2 [lindex $words 1]
   set firsts($word) $word2

   for {set i 0} {$i < [llength $words]} {incr i} {
      set word [lindex $words $i]
      set word2 [lindex $words $i+1]
      set word3 [lindex $words $i+2]

      if {$word2 ne ""} {
         lappend nexts($word\ $word2) $word3
      }
   }
}

proc lrandom L {
   lindex $L [expr {int(rand()*[llength $L])}]
}

proc say_word {word word2} {
   global nexts

   set next [lrandom $nexts($word\ $word2)]
   if {$next ne ""} {
      return "$next [say_word $word2 $next]"
   }
}

proc nicks2tags _sentence {
   global peernames
   upvar $_sentence sentence
   set substlist {}

   foreach name $peernames {
      set prefix {\m}
      set postfix {\M}
      set num [regsub -all -- "$prefix$name$postfix" $sentence <nick> sentence]
      if {$num > 0} { lappend substlist $name }
   }
   return $substlist
}


proc tags2nicks {_sentence args} {
   global peernames
   upvar $_sentence sentence

   foreach nick $args {
      regsub -- "<nick>" $sentence $nick sentence
   }

   while 1 {
      set random_nick [lrandom $peernames]
      set num [regsub -- "<nick>" $sentence "$random_nick" sentence]
      if {$num == 0} { break }
   }
}

proc say_sentence {} {
   global firsts

   set word [lrandom [array names firsts]]
   set word2 [lrandom $firsts($word)]

   if {$word2 ne ""} {
      set sentence "$word $word2 [say_word $word $word2]"
   } else {
      set sentence $word
   }

   tags2nicks sentence

   return $sentence
}

learn "I like  you"
puts [receive "jas!"]

# I am alive!
